---
title: Skills
date: 2020-04-05 16:00:34
slug: skills
---

## <u>`Technical`</u>

* Python | Go | Rust
* FastAPI | Flask | Django
* Vue js | React
* Pandas
* Airflow | Prefect
* HTML | JavaScript | JQuery | CSS
* SQL/Postgres and ORM
* Docker
* Git
* Testing frameworks like nose
* Beginner in the field of Machine Learning
* Java
* Zola | Hugo | Gridsome

## <u>`Non-Technical`</u>

* Trustworthy
* Independent
* Effective communication
* Team player
* Experience in Teaching
* Strong problem solver
* Good time management
