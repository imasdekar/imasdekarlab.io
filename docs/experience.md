---
title: Experience
date: 2020-04-05 16:00:34
slug: experience

---
##  <u>`UBS`</u>
<h3> Automation Solution and Python Developer</h3>
<h4>(January 2018 -  Present)</h4>

+ I worked on transforming risk reporting efficiency by developing full-stack solutions using Python (fastapi) & React, to automate processes for thousands of employees, cutting report turnaround from 24 hours to real-time.

+ I was a core member of the team to develop a solution for model documentation by automating Jupyter notebook-to-PDF conversion using Python & Pandoc, reducing manual effort and ensuring regulatory compliance.

+ I was a core member of the team to develop a Flask-based reporting system for Risk Weighted Assets (RWA), providing real-time regulatory insights and reducing manual workload.

+ I optimized financial risk monitoring by designing ETL pipelines in Airflow and Python, automating shortfall and limit calculations—mission-critical during the pandemic.

+ I developed a high-impact regulatory reporting system using FastAPI & Vue. This eliminated manual report tracking and reducing regulatory risks and time to minutes from days.

+ I worked single-handedly and in team for several business-critical applications by developing internal platforms for FAQs, policy processing, and automation using FastAPI & Vue, improving operational efficiency.

+ I implemented document generation with a GitLab Pages-based static website approach, which enabled seamless documentation submission and reduced manual documentation time and effort.

+ I deployed and managed multiple Azure-hosted Linux server applications with systemd and Nginx, ensuring 99.9% uptime & uninterrupted business operations.

+ I leveraged AI for sentiment analysis with VADER & completed ML in Finance certification to deepen expertise.

+ I integrated Markdown editing into business applications using StackEdit.js, improving content collaboration and document structuring.

+ I worked towards agile transformation by independently managing and working within teams for multiple high-stakes projects, refining client engagement, and optimizing collaboration with cross-functional teams.

+ I increased team efficiency by automating daily workflows using Python’s Doit library, freeing up developer time for high-value tasks.

+ I shared my knowledge with others through Python workshops, upskilling colleagues, and fostering an engineering-driven culture.

+ I showcased innovation and leadership by winning a company-wide team based hackathon and hosting another, reinforcing UBS’s position as a fintech leader. 

+ I was recognized for excellence through 2 promotions, highlighting my strategic contributions to the organization.



## <u>`Blenheim Chalcot`</u>
<h3> Python Developer</h3>
<h4>(February 2017 -  January 2018)</h4>

+ I worked in a team to develop and scale m-commerce APIs using Flask, to enable seamless integrations for Android & iOS applications.

+ I worked towards optimizing database operations by managing PostgreSQL and automating migrations with Alembic & SQLAlchemy, improving system scalability.

+ I worked on implementing AWS Lambda-based image compression, accelerating content delivery via S3.

+ I worked in an agile product development cycle through to deliver features faster thereby improving stakeholder alignment.

+ I initiated and executed software licensing strategies, ensuring legal protection and code compliance.



## <u>`Digital Freedom Foundation (India)`</u>
<h3> Project Manager | Developer</h3>
<h4>(August 2014 - February 2017)</h4>

+ I led the evolution of GNUKhata, transforming it from a desktop to a web-based open-source accounting solution using Python.

+ I orchestrated large-scale software development by managing project roadmaps, coordinating developers, and ensuring seamless feature rollouts.

+ I imparted hands-on training workshops to 1000+ users and developers, driving adoption and fostering a thriving open-source community.

+ I managed the open-source community and thereby incorporated user feedback and refined the software’s core functionality.
