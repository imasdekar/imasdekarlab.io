---
title: Education
date: 2020-04-05 16:00:34
slug: education
---

## <u>`Bachelor of Science in Information Technology`</u>
<h3> Mumbai University</h3>
<h4>(April 2013)</h4>




## <u>`Higher Secondary Certificate`</u>
<h3> Maharashtra State Board</h3>
<h4>(February 2010)</h4>




## <u>`Secondary School Certificate`</u>
<h3> Maharashtra State Board</h3>
<h4>(March 2008)</h4>
