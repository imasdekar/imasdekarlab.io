---
title: About
date: 2020-04-05 16:00:34
slug: about
---

<p><i><b>As a Full Stack Software Developer, I specialize in developing scalable solutions that drive efficiency and innovation. Proficient in Python and a spectrum of web technologies, I have a proven track record of delivering robust applications that meet complex business needs.
</p></i>

<p><i>I have sufficient expertise and exposure in the startup culture as well
as the field of Free and Open Source Software, allowing me to work
independently, across teams, and in a variety of roles.</p></i>

## <u>`YouTube Channel`</u>

> Name: [ITcracy](https://www.youtube.com/channel/UC6_qlLt9q-o3K5E0Un6eCYw)

> You can also find me on [YouTube](https://www.youtube.com/channel/UC6_qlLt9q-o3K5E0Un6eCYw), where my
software developer friend [Navin Karkera](https://navinkarkera.github.io) and I
share our expertise in programming languages and other technologies.


## <u>`Personal Details`</u>

- Location: &nbsp Mumbai, Maharashtra, India
- Phone: &nbsp <a class="resume-link" href="tel:+91 8082686033">+91 8082686033</a>
- Email: &nbsp <a href="mailto:ishan@disroot.org">ishan@disroot.org</a>


## <u>`Languages`</u>
- English
- Marathi
- Hindi


## <u>`Interests`</u>
- Martial Arts
- Music
- Movies
- Photography
- Cricket
- Foodie
- Trekking
- Travelling
